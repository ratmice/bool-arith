#![feature(const_fn)]
pub mod boolean_arith {
  use std::ops::{BitOr,BitAnd,Not};
  use std::ops::{Add,Mul};

  #[derive(Ord, PartialOrd, PartialEq, Eq, Clone, Copy, Debug)]
  pub struct LBool<T>(T);

  pub const fn mk_lit<T: BitOr + BitAnd + Not + Copy>(it:T) -> LBool<T> {
    LBool (it)
  }

  impl<T>
   Add for LBool<T>
    where T: Copy
           + BitOr<Output=T>
           + BitAnd
           + Not {
      type Output = LBool<T>;

      fn add(self, other: LBool<T>) -> Self { 
         LBool (::std::ops::BitOr::bitor(self.0, other.0))
      }
  }

  impl<T>
   Mul for LBool<T>
    where T: Copy
           + BitOr
           + BitAnd<Output=T>
           + Not {
      type Output = LBool<T>;

      fn mul(self, other: LBool<T>) -> Self {
         LBool (::std::ops::BitAnd::bitand(self.0, other.0))
      }
  }

  impl<T>
   Not for LBool<T>
    where T: Copy
           + BitOr
           + BitAnd
           + Not<Output=T> {
      type Output = LBool<T>;

      fn not(self) -> Self {
         LBool (::std::ops::Not::not(self.0))
      }
  }

  pub const LTRUE : LBool<bool> = mk_lit(true);
  pub const LFALSE: LBool<bool> = mk_lit(false); 
}

#[cfg(test)]
mod tests {
    use boolean_arith::mk_lit;
    use boolean_arith::{LTRUE,LFALSE};

    
    #[test]
    fn test_plus() {
       assert!(LTRUE + LFALSE == LTRUE);
       assert!(LFALSE + LFALSE == LFALSE);
       assert!(LFALSE + LTRUE == LTRUE);
       assert!(LTRUE + LFALSE == LTRUE);
    }

    #[test]
    fn test_mult() {
       assert!(LFALSE * LFALSE == LFALSE);
       assert!(LFALSE * LTRUE == LFALSE);
       assert!(LTRUE * LFALSE== LFALSE);
       assert!(LTRUE * LTRUE == LTRUE)
    }

    #[test]
    fn test_not() {
       assert!(LTRUE == !LFALSE);
       assert!(LFALSE == !LTRUE);
       assert!(!!LTRUE == LTRUE);
       assert!(!!LFALSE == !LTRUE);
    }

    #[test]
    fn test_mklit() {
       let ff = mk_lit(0u32);
       let ft = mk_lit(1u32);
       let tf = mk_lit(2u32);
       let tt = mk_lit(3u32);
       assert!(ft + tf == tt);
       assert!(ft * tf == ff);
       assert!(ft + tt == tt);
       assert!(ft * tt == ft);
    }
}
